import freenect
import time
import signal
import cv
import frame_convert

class Kinector:
    
    def __init__(self):
        self._ctx = freenect.init()

    def get_work(self):
        return [{'id':1234, 'tilt':30},
                {'id':4321, 'tilt':0}] 
    
    def post_result(self, result):
        print 'post:'+str(result)
        while 1:
            cv.ShowImage('Star', result['image'])
            if cv.WaitKey(10) == 27:
                break
            freenect.sync_stop() 
            
    def tilt(self, tilt):
        dev = freenect.open_device(self._ctx, freenect.num_devices(self._ctx)-1)
        freenect.set_tilt_degs(dev,tilt)
        time.sleep(3)
        freenect.close_device(dev)

    def start(self):
        work = self.get_work()
        for task in work:
            self.tilt(task['tilt'])
            task['image'] = frame_convert.video_cv(freenect.sync_get_video(freenect.num_devices(self._ctx)-1)[0])
            result = self.post_result(task)
    
    
if __name__=='__main__':
    kinector = Kinector()
    kinector.start()